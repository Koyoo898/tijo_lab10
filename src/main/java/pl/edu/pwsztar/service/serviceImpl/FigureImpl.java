package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.FigureService;

@Service(value = "figureService")
public class FigureImpl implements FigureService {
    @Override
    public boolean checkLauferMove(FigureMoveDto figureMoveDto){
        final char[] start = figureMoveDto.getStart().replace("_","").toCharArray();
        final char[] destination = figureMoveDto.getDestination().replace("_","").toCharArray();
        return Math.abs(-destination[0] + start[0]) == Math.abs(destination[1] - start[1]);
    }
}
